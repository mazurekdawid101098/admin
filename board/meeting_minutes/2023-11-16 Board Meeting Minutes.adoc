= F-Droid Board Meeting Minutes
F-Droid Board <board@fdroid.net>
:revdate: 2023-11-16 16:00 UTC

== Attendees

=== Board Members
Morgan Lemmer Webber, Michael Downey, Andrew Lewman, Hans-Christoph Steiner, Matthias Kirschner, John Sullivan

=== Guests
Fay, Izzy, Sylvia, linsui, uniq, Licaeon 

* Meeting Locationfootnote:[https://meet.calyx.net/OverallStrokesDeriveAutomatically]
* Previous meeting's notesfootnote:[https://pad.riseup.net/p/fhapnwz88Prwa1EM5z7]

Community Engagement: This meeting is open to all members of the F-Droid community and is subject to the F-Droid Code of Conductfootnote:[https://f-droid.org/en/docs/Code_of_Conduct/]. Morgan Lemmer-Webber will moderate, so please raise your hand in the conference call to be added to the queue. If you would like to propose an agenda item, please add it under the "Community Engagement" section at the end of the agenda below.

== Agenda                          
. Approve minutes from previous meeting
.. issue 11 in board/official https://gitlab.com/fdroid/board/official/-/issues/11
. Executive Session: The merit of board decisions on project operations, particularly as they pertain to the Community Council
. Figure out a plan for The Commons Conservancy lack of response
.. Plan A: continue along and wait
.. Plan B: Find a new sponsoring organization in EU
.. Plan C: Start F-Droid charity in EU
. CoC/Community Council Update
.. Due to scheduling conflicts and lack of access we have been delayed
.. Set up meeting with Gabriel from FSFE CARE team to support the F-Droid CoC team.
. Follow up on legal request(s) per mailing list
.. Discuss how to engage w/Commons Conservancy & agreement processes
... Plan B if TCC doesn't respond in a month?
.. SFLC India Letter of Engagement
.. UK takedown
.. Hetzner takedown notice: https://gitlab.com/fdroid/admin/-/issues/428
.. US States' Attornies General lawsuit
. New Board Member election process
.. 'board onboarding' document: https://pad.riseup.net/p/apbK4erae-oJyAYW1SK1-keep
. Set up subcommittees
.. Procedure for board to serve as mediators for community when needed
... Q: How to handle & resolve the existing/current COC incident 
.. Fundraising/Crowdfunding Campaign
... Goals
... Create Educational materials to explain the basics of what FDroid is, why it's important, and how you can use it to a general audience
.. Governance subcommittee
. Transferring domain names to board control.
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
. Following up on trademark progress
.. Engagement letter needed to be signed by commons conservancy
. Updates on budget (MD)
.. https://monitor.f-droid.org/donations
.. Our balance is $108,168.79. Donations were about near typical for liberapay and  Open Collective in October.
.. Average liberapay donation for the trailing 12 months is $1486.72. Average Open Collective donation for the trailing 12 months is $1390.89.
.. On OpenCollective, why is this the case? "Contributions over 500€/$ are not counted to the (monthly) total." Are they not all donations?
. Improving internal communication from the current fragmented situation (not everyone being on matrix/XMPP/forum/mail threads/etc.), having a channel for announcements for the team & board (both public and private) that everyone has easy access to (maybe gitlab works as an interim solution). Use discourse for non-development discussions/announcements maybe?
.. The F-Droid Board appears to all be on Matrix already.
. Community Engagement (Community members can propose agenda items below)
.. Has this been resolved? F-Droid board group (https://forum.f-droid.org/g/board?asc=true&order=) was created as per https://gitlab.com/fdroid/admin/-/issues/408 but many board members are still missing. Please give a forum admin (Sylvia / Licaon_Kter) your forum username
.. New Google Play policy, D-U-N-S registration for organisations. What are we going to do with the "Free Your Android" account publishing F-Droid Nearby? See https://play.google.com/store/apps/details?id=org.fdroid.nearby and https://chaos.social/@SylvieLorxu/110827167687326238 (summary). Which name will we use? Who will be responsible? Or will we let the account die? Also we may want to update https://f-droid.org/en/docs/FAQ_-_General/#why-isnt-f-droid-on-google-play based on decisions.
.. How and when should community members propose agenda items? The bot alerts a week before the meeting in the #fdroid-dev chat, but the agenda items were accidentally removed this time.
... Long-term: We should set up a forum where community members can propose agenda items and contribute asynchronously.
.. Clarify scope of CoC and Community Council mandate https://gitlab.com/fdroid/fdroid-website/-/merge_requests/962#note_1562507061
.. Clarify procedures core contributors must follow to gain access to various accounts and internal teams (e.g. outreach and the official mastodon account) and document current access and members. Also to avoid inconsistent application as it would be unfortunate if some team members are required to jump through more hoops than others for the same access.
.. https://gitlab.com/fdroid/admin/-/issues/401 needs to be extended again.
.. Who should get the board meeting announcement email? Just Sylvia and the board?
... For now, include Izzy and Fay, TBD best method long term for announcements

== Votes
. Morgan moves to approve meeting minutes from last meeting.
.. Michael seconds.
.. Discussion about what content to include in case we have to handle confidential matters.
.. Approved.

. Michael moves to adjourn the meeting to executive session.
.. Morgan seconds.
.. Approved.

. Executive session lasts 1 hour and 18 minutes.

. Morgan moves to invite Hans to the executive session.
. Matthias seconds.
. Approved.

. Morgan moves to end executive session and return to the board meeting
.. Matthias seconds.
.. Approved.

. Morgan moves to grant all community council members access be raised to maintainer status in Gitlab with the consideration that status shall be used for moderation purposes and not overall maintainer of all F-Droid gitlab repositories. Morgan will act as F-Droid Board representative without a Board meeting due to any abuse of privileges by the community council.
. All second.
. Approved.

. Morgan moves to grant all access to community council by Monday, 23 November 2023, subject to terms written by the F-Droid Board by 20 November 2023.
. Andrew seconds.
. Approved.

 Meeting ends at 18:30 UTC.


