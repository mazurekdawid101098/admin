= F-Droid Board Meeting Minutes
F-Droid Board <board@fdroid.net>
:revdate: 2023-06-15 16:00 UTC

== Attendees

=== Board Members
Morgan Lemmer Webber, Hans-Christoph Steiner, Andrew Lewman, John Sullivan, Michael Downey, Max Mehl

=== Guests
Sylvia, uniq, Izzy, FC, Danny

* Meeting Locationfootnote:[https://meet.calyx.net/OverallStrokesDeriveAutomatically]
* Previous meeting's notesfootnote:[https://pad.riseup.net/p/fhapnw0z88Prwa1EM5z7]

Community Engagement: This meeting is open to all members of the F-Droid community and is subject to the F-Droid Code of Conductfootnote:[https://f-droid.org/en/docs/Code_of_Conduct/]. Morgan Lemmer-Webber will moderate, so please raise your hand in the conference call to be added to the queue. If you would like to propose an agenda item, please add it under the "Community Engagement" section at the end of the agenda below.

== Agenda
. Status update on legal request(s) per mailing list
.. designating a point of contact?
.. SFLC India Letter Of Engagement (Michael and Hans)
.. review, approval/sign-off on letter sent 2023-06-08
. Procedure to enforce the CoC and a dedicator member who can perform it.
.. Recent failures in CoC enforcement, making sure marginalized community members can feel safe (again), restoring damaged trust, procedural improvements + engagement with the community to make sure this does not happen again.
. Following up on trademark progress 
.. Engagement letter (send to board, Morgan will sign on behalf of board, needs to be signed by commons conservancy)
. Updates on budget-tracking (MD)
.. https://monitor.f-droid.org/donations
. Discuss proposed repo for tracking disbursement requests
. Follow up on workflows for agenda (Morgan), minutes (Andrew), community engagement etc.
.. Use the same riseup pad each month and post link somewhere public (docs, wiki)
. Transferring domain names to board control.
.. Create an accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
. In-person meeting in Aberdeen, Scotland
.. Core dates: June 27-28
. Approve minutes from previous meeting
. Community Engagement (Community members can propose agenda items below)
. RFC: Client maintenance sponsored by the Calyx Institutefootnote:[https://gitlab.com/fdroid/admin/-/issues/388].


== Votes
. no motions

== Notes on Agenda:
. Status update on legal request(s) per mailing list
..designating a point of contact?
.. SFLC India Letter Of Engagement (Michael and Hans)
.. Hans spoke with 2 lawyers, they're interested in taking it on
.. There is no deadline on letter (so it's not urgent
.. We don't have much presence in India, so recommendations from people in India is useful
.. John has had negative experiences with this organization, so cautions how deeply we collaborate
.. Fee section is blank on letter
.. Provide as little information to the police as possible while complying with legal requirements (JS and HC, +1 MLW)
.. Could be value in publishing something publicly on the website about our privacy/data retention/GDPR policies overall that we can point this and future similar requests to (Uniq has started cataloging, Fdroid doesn't store a lot of PII -- just email, forums, and gitlab (but under gitlab ToS), so we can't really provide any of the info they requested except acces logs with country codes) ** wait for this action item until after we deal with this situation so we can incorporate what we learn **
.. reach out and clarify if they're doing it pro-bono or if there will be hidden fees
.. If we sign the letter we'll be covered by attorney client privelege
.. Respond with your opinions asynchronously via the email thread within one week, then we will email them with clarifying questions before signing
.. review, approval/sign-off on letter sent 2023-06-08
. DMCA request (UK organization, citing US law, to a dutch organization) -- we're protected under Dutch law for this, they're not accusing Fdroid of distributing copyrighted materials, this seems like something they should bring up with newpipe developers
.. Parallel case with youtube-dlfootnote:[https://github.blog/2020-11-16-standing-up-for-developers-youtube-dl-is-back/]footnote:[https://github.blog/2023-02-13-yout-amicus-fighting-for-developers-right-to-innovate/]
.. Google got a similar DMCA request about newpipefootnote:[https://www.androidheadlines.com/2023/05/google-bans-downloader-app-for-something-that-chrome-also-does.html]?
.. Action: Reach out to EFF (JS)
.. Is there any value to registering to DMCA (as a non-us based org)? https://gitlab.com/fdroid/admin/-/issues/359
.. Once this incident has passed, look into writing a copyright policy to publish publicly on the website that we can point to for future incidents (or part of the broader privacy policy revamp) https://gitlab.com/fdroid/admin/-/issues/381
.. Add any further input to the email thread asynchronously
. Procedure to enforce the CoC and a dedicator member who can perform it.
.. Recent failures in CoC enforcement, making sure marginalized community members can feel safe (again), restoring damaged trust, procedural improvements + engagement with the community to make sure this does not happen again.
.. Action: Set up CoC policies and procedures (MLW) -- Deadlines: will start thread on gitlab for community buy-in this week, will pose as an agenda at in person meeting, will commit to a long-term deadline at in person meeting
.. At least a team of at least 2-4 CoC enforcers (as much diversity in the team as possible, particularly for marginalized members of the community)
.. Make sure procedure is clearly posted for confidential reporting of CoC violations as well as confidential discussions of enforcement (look into tools we can use for this: https://github.com/scidsg/hush-line )
.. Make sure that the way we report things maintains confidentiality and trust of the person who reports and the victim
.. make sure we get community consensus before an immediate crisis happens so that enforcement team knows what can be done immediately
.. Training for team members (https://otter.technology/code-of-conduct-training/ but try to accommodate a more virtual community rather than event-based in person communities)
.. Input from community (who has been involved in the past, who is invested now?
.. Moderators on gitlab (how can we ban people from public issues on gitlab since that is a third party?), matrix, irc etc (is there a map of what spaces Fdroid is taking responsibility for?)
.. Action: do we want to self-host GitLab -- which would allow us better options for moderation (Uniq has been arguing for this for years, but that migration will be painful and will effect our operations severely, see https://gitlab.com/fdroid/admin/-/issues/159
.. Moderator guide (Andrew started, needs work)
.. There's a start at mapping access for chat, which could expand to everywhere we have presence, https://gitlab.com/fdroid/admin/-/issues/335
.. Immediate interim policy: initial decisive temporary (72 hours) freeze to stop immediate harassment and collect community feedback during that time (and lazy consensus ~1 week)
.. generally agreed from those in the meeting
. Following up on trademark progress 
.. Engagement letter (send to board, Morgan will sign on behalf of board, needs to be signed by commons conservancy)
. Updates on budget-tracking (MD)
.. https://monitor.f-droid.org/donations
.. Discuss proposed repo for tracking disbursement requests
. Follow up on workflows for agenda (Morgan), minutes (Andrew), community engagement etc.
.. Use the same riseup pad each month and post link somewhere public (docs, wiki)
. Transferring domain names to board control.
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
. In-person meeting in Aberdeen, Scotland
.. Core dates: June 27-28
. Approve minutes from previous meeting
. Community Engagement (Community members can propose agenda items below)
. RFC: Client maintenance sponsored by the Calyx Institute (https://gitlab.com/fdroid/admin/-/issues/388)